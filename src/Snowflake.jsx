import React from 'react';
import PropTypes from 'prop-types';
import './snowflakes.css';

function Snowflake(props) {
	const snowflakes = [];
	for(let i = 0; i < props.counter; i += 1) {
		const symbol = i % 4 ? '❅' : '❉';
		snowflakes.push(<span className="snowflakes__item" key={i}>{symbol}</span>);
	}	
	return (
		<div class="snowflakes">
			{snowflakes}
		</div>
	);
}

Snowflake.propTypes = {
	counter: PropTypes.number.isRequired
}

export default React.memo(Snowflake);